starship init fish | source 
export PATH="$HOME/.emacs.d/bin:$PATH"

# The fish greeting will be done with a snarly quote from the fortunes package
function fish_greeting
         fortune -a
end

function fish_user_key_bindings
	fish_vi_key_bindings
end

# Created by `pipx` on 2023-06-27 12:09:36
set PATH $PATH /home/nabarunr3/.local/bin
