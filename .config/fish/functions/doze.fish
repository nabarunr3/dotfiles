# Defined in - @ line 1
function doze --wraps='systemctl suspend' --description 'alias doze=systemctl suspend'
  systemctl suspend $argv;
end
