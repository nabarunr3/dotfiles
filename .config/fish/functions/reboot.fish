# Defined in - @ line 1
function reboot --wraps='systemctl reboot' --description 'alias reboot=systemctl reboot'
  systemctl reboot $argv;
end
