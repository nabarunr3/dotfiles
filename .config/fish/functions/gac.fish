# Defined in - @ line 1
function gac --wraps='git add --a && git commit -m' --description 'alias gac=git add --a && git commit -m'
  git add --a && git commit -m $argv;
end
