;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Nabarun Roy"
      user-mail-address "nabarunr3@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-outrun-electric)
;; (setq doom-acario-dark-brighter-modeline nil)
;; (setq doom-outrun-electric-brighter-modeline nil)
;; (setq doom-outrun-electric-padded-modeline nil)
;; (setq doom-outrun-electric-brighter-comments nil)


;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(use-package! vim-empty-lines-mode
  :custom
  (global-vim-empty-lines-mode t))

;;Face attributes
;;Set font when system is laptop (host is elitebook)
(when (string= (system-name) "nabarun-elitebook")
  (set-face-attribute 'default nil :font "Iosevka" :weight 'normal :height 125))
;;Set font when system is desktop (host is H81M)
(when (string= (system-name) "nabadesktop")
  (set-face-attribute 'default nil :font "Iosevka Fixed SS18" :weight 'normal :height 125))

;;Modeline
(use-package! doom-modeline
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-height 20)
  (doom-modeline-default-coding-system 'utf-8)
  (doom-modeline-major-mode-color-icon t))

;;Set window transparency
(use-package! transwin
  ;; :init
  ;; (transwin-toggle)

  )


;; ;;add indentation to programming modes
;; (use-package! highlight-indent-guides
;;   :hook
;;   (prog-mode . highlight-indent-guides-mode))


;;Org-Mode Config Starts
;;Use some padding for org-mode
(defun dawi/org-mode-setup ()
  (org-indent-mode)
  (visual-line-mode 1))

(use-package! org
  :hook (org-mode . dawi/org-mode-setup)
  :config
  ;;This shows the symbol "⇓" at the end of collapsed headers,
  ;;instead of the three dots ... The symbol can be replaced with
  ;;any symbol of choice.
  (setq org-ellipsis " ⇓")
;;   This hides the markup for bold, italics, underline and others.
;;  (setq org-hide-emphasis-markers t)
  (setq org-insert-heading-respect-content nil)
  )

  ;;Centering of text so that it looks good in fullscreen mode
  (defun dawi/org-mode-visual-fill ()
    (setq visual-fill-column-width 100
          visual-fill-column-center-text t)
    (visual-fill-column-mode 1))

  (use-package! visual-fill-column
    :hook (org-mode . dawi/org-mode-visual-fill))


;;We'll get rid of the stars in org headings
(use-package org-bullets
  :after org
x  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("⮕" "⦾" "→" "•" "↣")))

;;We'll also define the size of various headings, for easier demarcation
(with-eval-after-load 'org-faces
(dolist (face '((org-level-1 . 1.40)
		(org-level-2 . 1.20)
		(org-level-3 . 1.15)
		(org-level-4 . 1.05)
		(org-level-5 . 1.00)
		(org-level-6 . 0.95)
		(org-level-7 . 0.90)
		(org-level-8 . 0.85)))
  (set-face-attribute (car face) nil :font "Noto Sans Mono" :weight 'regular :height (cdr face)))
;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
(set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch))

;;Images
(setq org-display-inline-images t)
(setq org-startup-with-inline-images t)

;;Subscrips and superscripts
;;Org should display sub and superscripts as we type them
;;(setq org-pretty-entities t)
;;(setq org-pretty-entities-include-sub-superscripts t)
;;This sets sub and superscripts to display only when we surround the text succeeding _ and ^ in curly braces {}
(setq org-use-sub-superscripts "{}")

;;Dealing With Links
;;Links Preview
(defun my/link-preview ()
    (interactive)
    (let ((display-buffer-alist
           '(;; Bottom side window
             (".*"
              (display-buffer-in-side-window)
              (window-height . 0.25)
              (side . bottom)
              (slot . 0)))))
      ;;link opens at point but cursor moves there
      (org-open-at-point)
      ;;cursor moved back to working window
      (previous-window-any-frame)))
(global-set-key (kbd "C-S-p") 'my/link-preview)

;;Replaces Links by Description
(defun afs/org-replace-link-by-link-description ()
    "Replace an org link by its description or if empty its address"
  (interactive)
  (if (org-in-regexp org-link-bracket-re 1)
      (save-excursion
        (let ((remove (list (match-beginning 0) (match-end 0)))
              (description
               (if (match-end 2)
                   (match-string-no-properties 2)
                 (match-string-no-properties 1))))
          (apply 'delete-region remove)
          (insert description)))))

;;Replaces ALL links in the buffer
(defun afs/org-replace-all-links-by-description (&optional start end)
  "Find ALL org links and replace by their descriptions."
  (interactive
   (if (use-region-p) (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (save-excursion
    (save-restriction
      (narrow-to-region start end)
      (goto-char (point-min))
      (while (re-search-forward org-link-bracket-re nil t)
        (replace-match (match-string-no-properties
                        (if (match-end 2) 2 1)))))))

;;System-aware screenshot configuration [chungus]
(defun my/img-maker ()
 "Make folder if not exist, define image name based on time/date"
  (setq myvar/img-folder-path (concat default-directory "img/"))
  ; Make img folder if it doesn't exist.
  (if (not (file-exists-p myvar/img-folder-path)) ;[ ] refactor thir and screenshot code.
       (mkdir myvar/img-folder-path))
  (setq myvar/img-name (concat "img_" (format-time-string "%Y_%m_%d__%H_%M_%S") ".png"))
  (setq myvar/img-Abs-Path (concat myvar/img-folder-path myvar/img-name)) ;Relative to workspace.
  (setq myvar/relative-filename (concat "./img/" myvar/img-name)) ;For inserting into the current buffer
  (insert "[[" myvar/relative-filename "]]" "\n")
 )

(defun my/org-screenshot ()
  "Take a screenshot into a time stamped unique-named file in the
  sub-directory (%filenameIMG) as the org-buffer and insert a link
  to this file."
  (interactive)
  (my/img-maker)
;For GNU/Linux systems
 (when (eq system-type 'gnu/linux)
     ;(make-frame-invisible)
     (lower-frame)
     (call-process "import" nil nil nil myvar/img-Abs-Path)

     (raise-frame)
     ;(make-frame-visible)
     )
   ;For Windows Systems
   (when (eq system-type 'windows-nt)
      (shell-command "snippingtool /clip")
      (shell-command (concat "powershell -command \"Add-Type -AssemblyName System.Windows.Forms;if ($([System.Windows.Forms.Clipboard]::ContainsImage())) {$image = [System.Windows.Forms.Clipboard]::GetImage();[System.Drawing.Bitmap]$image.Save('" myvar/img-Abs-Path "',[System.Drawing.Imaging.ImageFormat]::Png); Write-Output 'clipboard content saved as file'} else {Write-Output 'clipboard does not contain image data'}\""))
      )
;; I'll bind this to the C-S-s and add to global-map to be able to call it
;; globally before the library is loaded
(global-set-key (kbd "C-S-s") 'my/org-screenshot)
(global-set-key (kbd "C-S-w") 'visual-line-mode)
(org-display-inline-images))

;;Table of (Contents) insertion in org-mode
(use-package! toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

;;Smartparens
(after! smartparens
  :config
  (require 'smartparens-config)
  (sp-pair "=" "=" :actions '(wrap))
  (sp-pair "+" "+" :actions '(wrap))
  (sp-pair "<" ">" :actions)
  (sp-pair "$" "$" :actions '(wrap))
  (sp-local-pair 'org-mode "\\[" "\\]")
  (sp-local-pair 'org-mode "$" "$")
  (sp-local-pair 'org-mode "'" "'" :actions '(rem))
  (sp-local-pair 'org-mode "=" "=" :actions '(rem))
  (sp-local-pair 'org-mode "\\left(" "\\right)" :trigger "\\l(" :post-handlers '(sp-latex-insert-spaces-inside-pair))
  (sp-local-pair 'org-mode "\\left[" "\\right]" :trigger "\\l[" :post-handlers '(sp-latex-insert-spaces-inside-pair))
  (sp-local-pair 'org-mode "\\left\\{" "\\right\\}" :trigger "\\l{" :post-handlers '(sp-latex-insert-spaces-inside-pair))
  (sp-local-pair 'org-mode "\\left|" "\\right|" :trigger "\\l|" :post-handlers '(sp-latex-insert-spaces-inside-pair))
  )

;adjusting scale of Org LaTeX preview images with display scale
(defun my/text-scale-adjust-latex-previews ()
  "Adjust the size of latex preview fragments when changing the
buffer's text scale."
  (pcase major-mode
    ('latex-mode
     (dolist (ov (overlays-in (point-min) (point-max)))
       (if (eq (overlay-get ov 'category)
               'preview-overlay)
           (my/text-scale--resize-fragment ov))))
    ('org-mode
     (dolist (ov (overlays-in (point-min) (point-max)))
       (if (eq (overlay-get ov 'org-overlay-type)
               'org-latex-overlay)
           (my/text-scale--resize-fragment ov))))))

(defun my/text-scale--resize-fragment (ov)
  (overlay-put
   ov 'display
   (cons 'image
         (plist-put
          (cdr (overlay-get ov 'display))
          :scale (+ 1.0 (* 0.25 text-scale-mode-amount))))))

(add-hook 'text-scale-mode-hook #'my/text-scale-adjust-latex-previews)

;;Org-roam config
(use-package! org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/NabarunKB")
  ;; (org-roam-dailies-directory "Daily_Journal/")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
    '(
      ;;Default capture templae
      ("d" "default" plain
       "%?"
       :if-new (file+head "main/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      ;;Protein/Gene capture template
      ("g" "Protein or Gene" plain
       (file "~/.emacs.d/machinery/RoamTemplates/ProteinNode.org")
       :if-new (file+head "main/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      ;;Curriculum Node
      ("c" "Curriculum Note" plain
       (file "~/.emacs.d/machinery/RoamTemplates/CurriculumNotes.org")
       :if-new (file+head "main/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      ;;Person Node
      ("p" "Person Note" plain
       (file "~/.emacs.d/machinery/RoamTemplates/PersonNode.org")
       :if-new (file+head "person/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
      )
    )

     ;; (org-roam-dailies-capture-templates
     ;;  '(("d" "default" entry "* %<%H:%M:%S %Z(%z)> %?"
     ;;     :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))

  :bind (
         ("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n t" . org-roam-tag-add)
         ("C-c n a" . org-roam-alias-add)
         :map org-mode-map
         ("C-S-i" . completion-at-point)
         ;; :map org-roam-dailies-map
         ;; ("Y" . org-roam-dailies-capture-yesterday)
         ;; ("T" . org-roam-dailies-capture-tomorrow)
         )

  ;; :bind-keymap
    ;; ("C-c n d" . org-roam-dailies-map)

  :config
  ;; (require 'org-roam-dailies) ;; Ensure the keymap is available
    (org-roam-db-autosync-mode))

;; Insert new nodes faster and bind this to C-c n m
(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))
(global-set-key (kbd "C-c n m") 'org-roam-node-insert-immediate)

;;Modifying zettel display
(with-eval-after-load 'org-roam
        (cl-defmethod org-roam-node-type ((node org-roam-node))
           "Return the TYPE of NODE."
           (condition-case nil
               (file-name-nondirectory
                (directory-file-name
                 (file-name-directory
                  (file-relative-name (org-roam-node-file node) org-roam-directory))))
             (error ""))
           ))
(setq org-roam-node-display-template
      (concat "${type:15} ${title:*} " (propertize "${tags:10}" 'face 'org-tag)))

;;Capture Idea and bind to C-q--
(setq org-capture-templates
      ;; other capture templates
      '(("s" "Slipbox" entry  (file "~/NabarunKB/capture/braindump.org")
	 "* %<%d-%m-%Y %H:%M:%S %Z(%z)> \n%?")))

(defun jethro/org-capture-slipbox ()
  (interactive)
  (org-capture nil "s"))
(global-set-key (kbd "C-q") 'jethro/org-capture-slipbox)

;;org-roam timestamps. Records the creation and last modifies times of files.
  (use-package! org-roam-timestamps
    :after org-roam
    :config
    (org-roam-timestamps-mode)
    (setq org-roam-timestamps-remember-timestamps t)
    (setq org-roam-timestamps-minimum-gap 3600))


;;org-roam-ui
(use-package! org-roam-ui
  ;;normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;a hookable mode anymore, you're advised to pick something yourself
  ;;if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
	org-roam-ui-follow t
	org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

;;org-agenda
;; To manualy set files to be tracked by org-agenda
(defun my/org-agenda-refresh-agenda-list ()
  (interactive)
    (setq org-agenda-files '("~/NabarunDocs/Planning/planning.org.gpg")))
;; Build the agenda list the first time for the session
(add-hook 'after-init-hook 'my/org-agenda-refresh-agenda-list)

;; setting log-mode in journal
(setq org-agenda-start-with-log-mode t)
(setq org-log-done 'time)
(setq org-log-into-drawer t)
;;(setq org-agenda-span 'day) ;;comment/uncomment if you want to view entire agenda for the week/day

(require 'org-habit)
(add-to-list 'org-modules 'org-habit)
(setq org-habit-graph-column 60)

;;Tracking Agenda Directly From Org-Roam files with a specific filetag called "Planning"
;; (defun my/org-roam-filter-by-tag (tag-name)
;;   (lambda (node)
;;     (member tag-name (org-roam-node-tags node))))

;; (defun my/org-roam-list-notes-by-tag (tag-name)
;;   (mapcar #'org-roam-node-file
;;           (seq-filter
;;            (my/org-roam-filter-by-tag tag-name)
;;            (org-roam-node-list))))

;; (defun my/org-roam-refresh-agenda-list ()
;;   (interactive)
;;   (setq org-agenda-files (my/org-roam-list-notes-by-tag "Planning")))

;; ;; Build the agenda list the first time for the session
;; (my/org-roam-refresh-agenda-list)

;; Bind opening of a particular file containing my journal to C-S-j
(defun open-journal-file ()
  "Open a specific file containing journal entries."
  (interactive)
  (find-file "~/NabarunDocs/journal.org.gpg"))
(global-set-key (kbd "C-S-j") 'open-journal-file)

;;Deft to search org-roam files
(after! deft
    :config
    (setq deft-directory org-roam-directory
	  deft-extensions '("txt" "org")
          deft-recursive t
          deft-strip-summary-regexp ":PROPERTIES:\n\\(.+\n\\)+:END:\n"
          deft-use-filename-as-title t))

;;Org babel configs
;;For quick insertion of structure templates
(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("lt" . "src LaTeX"))
(add-to-list 'org-structure-template-alist '("R" . "src R"))

;;Function to close the current session, if the block is using one [chungus]
(defun kitchin/org-babel-kill-session ()
  "Kill session for current code block."
  (interactive)
  (unless (org-in-src-block-p)
    (error "You must be in a src-block to run this command"))
  (save-window-excursion
    (org-babel-switch-to-session)
    (kill-buffer)))

;;Clearing all results from the buffer
(defun kitchin/org-babel-remove-result-buffer ()
  "Remove results from every code block in buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward org-babel-src-block-regexp nil t)
      (org-babel-remove-result))))

;; Here is a function for testing if a block is in a session.
(defun kitchin/src-block-in-session-p (&optional name)
  "Return if src-block is in a session of NAME.
NAME may be nil for unnamed sessions."
  (let* ((info (org-babel-get-src-block-info))
         (lang (nth 0 info))
         (body (nth 1 info))
         (params (nth 2 info))
         (session (cdr (assoc :session params))))

    (cond
     ;; unnamed session, both name and session are nil
     ((and (null session)
           (null name))
      t)
     ;; Matching name and session
     ((and
       (stringp name)
       (stringp session)
       (string= name session))
      t)
     ;; no match
     (t nil))))

;;This function restarts the current session the block under point is in, and then re-evaluates all the blocks above that session
(defun kitchin/org-babel-restart-session-to-point (&optional arg)
  "Restart session up to the src-block in the current point.
Goes to beginning of buffer and executes each code block with
`org-babel-execute-src-block' that has the same language and
session as the current block. ARG has same meaning as in
`org-babel-execute-src-block'."
  (interactive "P")
  (unless (org-in-src-block-p)
    (error "You must be in a src-block to run this command"))
  (let* ((current-point (point-marker))
         (info (org-babel-get-src-block-info))
         (lang (nth 0 info))
         (params (nth 2 info))
         (session (cdr (assoc :session params))))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward org-babel-src-block-regexp nil t)
        ;; goto start of block
        (goto-char (match-beginning 0))
        (let* ((this-info (org-babel-get-src-block-info))
               (this-lang (nth 0 this-info))
               (this-params (nth 2 this-info))
               (this-session (cdr (assoc :session this-params))))
            (when
                (and
                 (< (point) (marker-position current-point))
                 (string= lang this-lang)
                 (kitchin/src-block-in-session-p session))
              (org-babel-execute-src-block arg)))
        ;; move forward so we can find the next block
        (forward-line)))))

;;Citation Management
;;We want to use the default citation manager used by org-roam version >=9.5.
;;For this we need to let both citar and org-cite know the location of our bibliography
;;Org cite
(setq org-cite-global-bibliography '("~/NabarunKB/biblio.bib"))
;;(setq org-styles)
;;Citar
(after! citar
  (setq! citar-bibliography '("~/NabarunKB/biblio.bib"))
  (setq! citar-notes-paths '("~/NabarunKB/reference"))

  :config
  (setq citar-templates
      '((main . "${author editor:30}     ${date year issued:4}     ${title:48}")
        (suffix . "          ${=key= id:15}    ${=type=:12}    ${tags keywords:*}")
        (preview . "${author editor} (${year issued date}) ${title}, ${journal journaltitle publisher container-title collection-title}.\n")
        (note . "Notes on ${author editor}, ${title}")))

  (setq citar-symbols
      `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green :v-adjust -0.1) . " ")
        (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) . " ")
        (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) . " ")))
  (setq citar-symbol-separator "  "))
;; bind citar-insert-citation to global-map to be able to call it
;; globally before the library is loaded
(define-key global-map (kbd "C-c b") #'citar-insert-citation)
(define-key global-map (kbd "C-c f") #'citar-open-files)

;;Notes from citations!
(use-package! citar-org-roam
  :config
  (setq citar-org-roam-subdir "reference"))

;;LaTeX settings
(require 'ox-latex)
(setq org-latex-inputenc-alist '(("utf8" . "utf8x")))
(setq org-latex-default-packages-alist (cons '("mathletters" "ucs" nil) org-latex-default-packages-alist))
(add-to-list 'org-latex-packages-alist '("" "listings" nil))
(setq org-latex-listings t)
(setq org-latex-listings-options '(("breaklines" "true")
                                   ("literate" "{0}{0}{1}%
           {1}{1}{1}%
           {2}{2}{1}%
           {3}{3}{1}%
           {4}{4}{1}%
           {5}{5}{1}%
           {6}{6}{1}%
           {7}{7}{1}%
           {8}{8}{1}%
           {9}{9}{1}%
    ")))

;;LaTeX preview pane
(use-package! latex-preview-pane
  :custom
  (latex-preview-pane-enable))

;; (use-package! peep-dired
;;   :config
;;   (setq peep-dired-cleanup-eagerly t)
;;   (evil-define-key 'normal peep-dired-mode-map
;;     (kbd "j") 'peep-dired-next-file
;;     (kbd "k") 'peep-dired-prev-file)
;;   (add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; ;;  (setq peep-dired-ignored-extensions '("mkv" "iso" "mp4"))
;;   )
;;
;;
;; open dired file in eww
(define-key dired-mode-map "e" (lambda () (interactive) (eww-open-file (dired-get-file-for-visit))))

;;Chatgpt
(use-package! chatgpt
  :defer t
  :config
  (unless (boundp 'python-interpreter)
    (defvaralias 'python-interpreter 'python-shell-interpreter))
  (setq chatgpt-repo-path (expand-file-name "straight/repos/ChatGPT.el/" doom-local-dir))
  (set-popup-rule! (regexp-quote "*ChatGPT*")
    :side 'bottom :size .5 :ttl nil :quit t :modeline nil)
  :bind ("C-c q" . chatgpt-query))

;;Org-remark
(org-remark-global-tracking-mode +1)
;; Key-bind `org-remark-mark' to global-map so that you can call it
;; globally before the library is loaded.

(define-key global-map (kbd "C-c n m") #'org-remark-mark)

;; The rest of keybidings are done only on loading `org-remark'
(with-eval-after-load 'org-remark
  (define-key org-remark-mode-map (kbd "C-c n o") #'org-remark-open)
  (define-key org-remark-mode-map (kbd "C-c n ]") #'org-remark-view-next)
  (define-key org-remark-mode-map (kbd "C-c n [") #'org-remark-view-prev)
  (define-key org-remark-mode-map (kbd "C-c n r") #'org-remark-remove))

;; GPTel
(use-package! gptel
  :config
  (setq! gptel-api-key "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")
  (setq! gptel-default-mode 'org-mode))

;; Themes would load according to a set time of the day (for some reason this does not work when put anywhere else)
;; (load-theme 'doom-acario-light t t) ;;load light theme
;; (run-at-time "06:00" (* 60 60 24) (lambda () (enable-theme 'doom-acario-light)))
;; (load-theme 'doom-outrun-electric t t) ;;load dark theme
;; (run-at-time "18:00" (* 60 60 24) (lambda () (enable-theme 'doom-outrun-electric)))
